package threadsUsingRunnable;

import java.util.concurrent.TimeUnit;

public class ThreadsUsingRunnableExample {
    public static void showExampleRunnable() {
        Runnable task1 = () -> doTask("_");
        Runnable task2 = () -> doTask("-");

        startTaskInThread(task1, task2);
    }

    public static void showExampleSleepingThread() {
        Runnable task1 = () -> doTaskWithSleep("_", 500);
        Runnable task2 = () -> doTaskWithSleep("-", 1000);

        startTaskInThread(task1, task2);
    }

    public static void showExampleSleepingThreadUsingTimeUnit() {
        Runnable task1 = () -> doTaskWithSleepUsingTimeUnit("_", 500);
        Runnable task2 = () -> doTaskWithSleepUsingTimeUnit("-", 1000);
        startTaskInThread(task1, task2);
    }

    public static void showExampleWithInterrupting() {
        Runnable task1 = () -> doTaskWithInterrupting("_", 500);
        Runnable task2 = () -> doTaskWithInterrupting("-", 1000);
        startTaskInThread(task1, task2);
    }

    public static void showExampleWithCheckingInterrupting() {
        Runnable task1 = () -> doTaskWithCheckingInterrupting("_");
        Runnable task2 = () -> doTaskWithCheckingInterrupting("-");
        startTaskInThread(task1, task2);
    }

    private static void startTaskInThread(Runnable task1, Runnable task2) {
        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);

        thread1.start();
        thread2.start();
    }

    private static void doTask(String delimiter) {
        for (int i = 1; i <= 50; i++) {
            if (i % 10 == 0)
                System.out.println();
            System.out.print(i + delimiter);
        }
    }

    private static void doTaskWithSleep(String delimiter, int millisecondsToSleep) {
        try {
            for (int i = 1; i <= 50; i++) {
                if (i % 10 == 0) {
                    Thread.currentThread().sleep(millisecondsToSleep);
                    System.out.println();
                }
                System.out.print(i + delimiter);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void doTaskWithSleepUsingTimeUnit(String delimiter, int millisecondsToSleep) {
        try {
            for (int i = 1; i <= 50; i++) {
                if (i % 10 == 0) {
                    TimeUnit.MILLISECONDS.sleep(millisecondsToSleep);
                    System.out.println();
                }
                System.out.print(i + delimiter);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void doTaskWithInterrupting(String delimiter, int millisecondsToSleep) {
        try {
            for (int i = 1; i <= 50; i++) {
                if (i % 10 == 0) {
                    TimeUnit.MILLISECONDS.sleep(millisecondsToSleep);
                    System.out.println();
                }
                System.out.print(i + delimiter);
                if (i % 30 == 0)
                    Thread.currentThread().interrupt();
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void doTaskWithCheckingInterrupting(String delimiter) {
        while(!Thread.currentThread().isInterrupted()) {
            for (int i = 1; i <= 50; i++) {
                if (i % 10 == 0) {
                    System.out.println();
                }
                System.out.print(i + delimiter);
                if (i % 30 == 0)
                    Thread.currentThread().interrupt();
            }
        }
    }
}
