package extendsThreads;

public class MySecondThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            if (i % 10 == 0) {
                System.out.println();
            }
            System.out.print(i+"-");
        }
    }
}
