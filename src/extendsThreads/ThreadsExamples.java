package extendsThreads;

public class ThreadsExamples {
    private static MyFirstThread myFirstThread = new MyFirstThread();
    private static MySecondThread mySecondThread = new MySecondThread();
    public static void showExample(){
        myFirstThread.start();
        mySecondThread.start();
    }
}
