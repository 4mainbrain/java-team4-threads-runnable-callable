package threadsUsingCallable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class ThreadsUsingCallableExample {

    public static void showExampleWithCallableAndFuture() throws ExecutionException, InterruptedException {
        Callable task = () -> concat("4", "5");

        FutureTask<String> futureTask = new FutureTask<>(task);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());
    }

    private static String concat(String a, String b){
        return a+b;
    }
}
