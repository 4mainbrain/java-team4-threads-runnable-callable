import threadsUsingCallable.ThreadsUsingCallableExample;
import threadsUsingRunnable.ThreadsUsingRunnableExample;

import java.util.concurrent.ExecutionException;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        ThreadsExamples.showExample();
//        ThreadsUsingRunnableExample.showExampleRunnable();
//        ThreadsUsingRunnableExample.showExampleSleepingThread();
//        ThreadsUsingRunnableExample.showExampleSleepingThreadUsingTimeUnit();
//        ThreadsUsingRunnableExample.showExampleWithInterrupting();
//        ThreadsUsingRunnableExample.showExampleWithCheckingInterrupting();
        ThreadsUsingCallableExample.showExampleWithCallableAndFuture();
    }
}
